import React, { Component } from 'react'
import { TextInput, View, StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/Feather'

export class CTextInput extends Component {
    render() {
        return (
            <View style={styles.input}>
                <Icon name={this.props.name} size={20} style={{ color: '#0174CF', marginRight: 10 }} />
                <TextInput placeholder={this.props.placeholder} />
            </View>
        )
    }
}

export default CTextInput

const styles = StyleSheet.create({
    input: {
        width: 270,
        paddingHorizontal: 15,
        borderWidth: 0.3,
        borderRadius: 50,
        marginTop: 20,
        marginHorizontal: 10,
        elevation: 5,
        backgroundColor: 'white',
        borderColor: '#0174CF',
        flexDirection: 'row',
        alignItems: 'center',
    }
})