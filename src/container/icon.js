import React, {Component} from 'react'
import {Text, View, StyleSheet, Image, TextInput, TouchableOpacity} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5'

class IconExercise extends Component{
  render(){
    return(
      <View>
        <Icon name="home" size={30} color='blue'/>
        <Icon name="backward" size={30} color='red' />
      </View>
    )
  } 
 }
 
 export default IconExercise
 