import React, { Component } from 'react'
import { Text, Image, View, StyleSheet, ScrollView } from 'react-native'
import gambar from '../assets/endgame.jpg'
import gambar1 from '../assets/antman.jpg'
import gambar2 from '../assets/doctorstrange.jpg'

class Cinema extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={{ color: 'white', fontSize: 20, marginBottom: 15 }}>Playing Now</Text>
                       <View style={styles.box}>
                        <Image source={gambar} style={styles.poster} />
                        <View style={{ flex: 1, justifyContent: 'space-between', padding: 10 }}>
                            <Text style={{ color: 'white', fontSize: 20 }}>Avenger End Game</Text>
                            <Text style={{ color: '#5C5B71', fontSize: 15 }}>Director : Joe Russo</Text>
                            <Text style={{ color: '#5C5B71', fontSize: 15 }}>Time : 182 min</Text>
                            <Text style={{ color: 'yellow', fontSize: 15 }}>Rating : 9.5/10 </Text>
                        </View>
                    </View>
            </View>

        )
    }
}

export default Cinema

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#17162E',
        padding: 15
    },
    box: {
        backgroundColor: '#1D1C3B',
        padding: 15,
        width: '100%',
        height: '30%',
        borderRadius: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 10
    },
    poster: {
        width: '30%',
        height: '100%',
        borderColor: '#5C5B71',
        borderRadius: 15,
        borderWidth: 0.5,
    }
})

