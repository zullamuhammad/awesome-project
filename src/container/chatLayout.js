import React, { Component } from 'react'
import { Text, View, StyleSheet, TextInput } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5'

export class ChatLayout extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.TopContent}>
                    <View style={{ height: 50, width: 50, borderRadius: 50, backgroundColor: '#83a958', justifyContent: 'center', alignItems: 'center' }}>
                        <Icon name='user' size={20} style={{ color: '##427322' }} />
                    </View>
                    <Text style={{ fontSize: 20 }}> Zulla Muhammad </Text>
                    <View style={{ height: 40, width: 40, borderRadius: 50, backgroundColor: '#83a958', justifyContent: 'center', alignItems: 'center' }}>
                        <Icon name='phone' size={15} style={{ color: '##427322' }} />
                    </View>
                    <View style={{ height: 40, width: 40, borderRadius: 50, backgroundColor: '#83a958', justifyContent: 'center', alignItems: 'center' }}>
                        <Icon name='video' size={15} style={{ color: '##427322' }} />
                    </View>
                </View>

                <View style={{ flex: 1, backgroundColor: '#d8e5c6', padding: 15 }}>
                    <View style={{ width: '50%', height: 40, backgroundColor: '#83a958', justifyContent: 'center', padding: 10, marginBottom: 15 }}>
                        <Text style={{ color: 'white' }}> Lagi apa? </Text>
                    </View>
                    <View style={{ width: '50%', height: 40, backgroundColor: '#427322', justifyContent: 'center', padding: 10, alignSelf:'flex-end' }}>
                        <Text style={{ color: 'white' }}> Kepo deh </Text>
                    </View>
                </View>

                <View style={styles.TopContent}>
                    <View style={styles.input}>
                        <TextInput placeholder={'Ketik Pesan'} style={{ marginHorizontal: 10 }} />
                    </View>
                    <View style={{ height: 40, width: 40, borderRadius: 50, backgroundColor: '#83a958', justifyContent: 'center', alignItems: 'center' }}>
                        <Icon name='video' size={15} style={{ color: '##427322' }} />
                    </View>
                </View>
            </View>
        )
    }
}

export default ChatLayout

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#d8e5c6',
    },
    TopContent: {
        backgroundColor: '#a3c185',
        padding: 15,
        width: '100%',
        height: '10%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    input: {
        width: '80%',
        height: '120%',
        borderWidth: 1,
        backgroundColor: '#d8e5c6',
        borderColor: '#1a320f',
    }

})
