import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TextInput, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/Feather'
import Logo from '../assets/logopkbm.png'
import CTextInput from '../component/CTextInput'

class LoginForm extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image source={Logo} style={styles.logo} />
        <Text style={{ color: '#0174CF', marginTop: 20 }}>Please login with a resgitered acccount</Text>

        <CTextInput name='user' placeholder='Username / Email'/>

        <View style={styles.input}>
          <Icon name='key' size={20} style={{ color: '#0174CF'}} />
          <TextInput secureTextEntry placeholder='Password' style={{flex: 1}} />
          <Icon name='eye-off' size={20} style={{ color: '#0174CF'}} />
        </View>

        <TouchableOpacity style={styles.button}>
          <Text style={{ color: '#fff' }}>LOGIN</Text>
        </TouchableOpacity>

        <Text style={{ marginTop: 20 }}>
          Forgot Password ? <Text style={{ color: '#0174CF' }}> Reset Password </Text>
        </Text>
        <Text style={{ marginTop: 10 }}>
          Don't have an account ? <Text style={{ color: '#0174CF' }}> Sign Up </Text>
        </Text>

        <Text style={{ color: '#0174CF', fontSize: 10, marginTop: 100 }}> Design by zullamuhammad</Text>
      </View>
    )
  }
}

export default LoginForm

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white'
  },
  logo: {
    width: 200,
    height: 200
  },
  input: {
    width: 270,
    paddingHorizontal: 15,
    borderWidth: 0.3,
    borderRadius: 50,
    marginTop: 20,
    marginHorizontal: 10,
    elevation: 5,
    backgroundColor: 'white',
    borderColor: '#0174CF',
    flexDirection: 'row',
    alignItems: 'center',
  },
  button: {
    width: 180,
    paddingVertical: 16,
    marginTop: 20,
    borderRadius: 50,
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#0174CF',
    elevation: 5,
    backgroundColor: '#0174CF'
  }
}

)