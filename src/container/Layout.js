import React, {Component} from 'react'
import {Text, View, StyleSheet, TextInput, TouchableOpacity} from 'react-native'

class LayoutExercise extends Component{
    render(){
        return(
            
            <View style={{flex: 1}}>
                <View style={{flex: 1, flexDirection: 'row'}}>
                    <View style={{flex: 1, backgroundColor: '#3498DB'}}/>
                    <View style={{flex: 1, backgroundColor: '#E74C3C'}}/>
                </View>
 
                <View style={{flex: 1, flexDirection: 'row'}}>
                    <View style={{flex: 1, backgroundColor: '#8E44AD'}}/>
                    <View style={{flex: 2, backgroundColor: '#F1C40F'}}/>
                    <View style={{flex: 1, flexDirection: 'column'}}>
                        <View style={{flex: 1, backgroundColor: '#27AE60'}}/>
                        <View style={{flex: 1, backgroundColor: '#E67E22'}}/>
                    </View>
                </View>

                <View style={{flex: 0.5, flexDirection:'column', backgroundColor: '#3498DB'}}/>

                <View style={{flex: 1, justifyContent: 'space-between', flexDirection: 'row'}}>
                    <View style={{width:'20%'}}>
                        <View style={{flex: 1, backgroundColor: '#E74C3C'}}/>
                        <View style={{flex: 1, backgroundColor: '#8E44AD'}}/>
                    </View>
                    <View style={{width:'20%', backgroundColor: '#27AE60'}}/>
                    <View style={{width:'20%', flexDirection: 'column'}}>
                        <View style={{flex: 1, backgroundColor: '#E74C3C'}}/>
                        <View style={{flex: 1, backgroundColor: '#8E44AD'}}/>
                    </View>
                </View>
                
                <View style={{flex: 1, padding: 30, backgroundColor:'#F1C40F', flexDirection:'row'}}>
                    <View style={{flex: 1}}>
                        <View style={{flex: 2, backgroundColor: '#E84393'}}/>
                    </View>
                    <View style={{flex: 1, flexDirection:'column'}}>
                        <View style={{flex: 1, backgroundColor: '#0984E3'}}/>
                        <View style={{flex: 1, backgroundColor: '#27AE60'}}/>
                    </View>
                    <View style={{flex: 0.5, backgroundColor: '#6C5CE7'}}/>
                </View>

                <View style={{flex: 0.5, justifyContent: 'space-around', flexDirection: 'row'}}>
                    <View style={{width: '20%', backgroundColor: '#636E72'}}/>
                    <View style={{width: '20%', backgroundColor: '#8E44AD'}}/>
                    <View style={{width: '20%', backgroundColor: '#D63031'}}/>
                </View>
            </View>
        )
    }
}


export default LayoutExercise