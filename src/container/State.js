import React, {Component} from 'react'
import {Text, View, StyleSheet, TextInput, TouchableOpacity} from 'react-native'

class StateExercise extends Component{

    constructor(){
        super()
        this.state = {
            name : 'zullamhammad',
            input : '',
        }
    }

    change =() => {
        this.setState({
            name : this.state.input,
            input : '',
        })
    }

    render(){
        return (
            <View style={styles.container}>
                <Text> {JSON.stringify(this.state)} </Text>
                <TouchableOpacity style={styles.button}>
                <Text style={{color: '#fff'}}> {this.state.name}</Text>
                </TouchableOpacity>
                <TextInput placeholder='Masukan Nama' style={styles.input} 
                onChangeText= {(ketikan)=>this.setState({input : ketikan})} value={this.state.input}/>
                <TouchableOpacity style={styles.button} onPress={this.change}>
                <Text style={{color: '#fff'}}> Enter </Text>
                </TouchableOpacity>
            </View>
        )
    }
}

export default StateExercise

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent:'center',
        alignItems:'center',
        color:'#ffae00',
        flex: 1
    },
    input:{
        width: 180,
        paddingHorizontal: 25,
        borderWidth: 0.3,
        borderRadius: 50,
        marginTop: 20,
        elevation: 5,
        backgroundColor: 'white',
        borderColor: '#0174CF'
    },
    button: {
        width: 180,
        paddingVertical: 16,
        marginTop: 20,
        borderRadius: 50,
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#0174CF',
        elevation: 5,
        backgroundColor: '#0174CF'
    },
    ketikan:{
        width: 180,
        paddingHorizontal: 25,
        borderWidth: 0.3,
        borderRadius: 50,
        marginTop: 20,
        elevation: 5,
        backgroundColor: 'white',
        borderColor: '#0174CF'
    }
})