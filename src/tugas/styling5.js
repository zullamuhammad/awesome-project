import React, { Component } from 'react'
import { Text, View } from 'react-native'

export class Styling5 extends Component {
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'blue', padding: 15 }}>
                <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center', marginBottom: 10 }}>
                    <Text style={{ color: 'white', fontSize: 20 }}> Parent Component </Text>
                </View>

                <View style={{ width: '100%', height: 180, backgroundColor: 'cyan', padding: 10, marginBottom: 20 }}>
                    <Text style={{ color: 'black', fontSize: 20, alignSelf: 'center', marginBottom: 10 }}> Child Component </Text>
                    <View style={{ width: '100%', height: 100,flexDirection:'row', justifyContent:'space-between' }}>
                        <View style={{ width: 100, height: 100, backgroundColor: 'red', borderRadius: 50, marginTop:15 }}>
                            <Text style={{ color: 'white', fontSize: 20, alignSelf: 'center', marginTop: 30}}> Child </Text>
                        </View>
                        <View style={{ width: '60%', height: 100, flexDirection: 'column'}}>
                            <View style={{ width: '100%', height: 80, backgroundColor: 'green', marginTop: 30}}>
                                <Text style={{ color: 'white', fontSize: 20, alignSelf: 'center', marginTop: 25}}> Child </Text>
                            </View>
                        </View>
                    </View>
                </View>

                <View style={{ width: '100%', height: 120, backgroundColor: 'cyan', alignSelf: 'center', padding: 10, marginBottom: 20 }}>
                    <Text style={{ color: 'black', fontSize: 20, alignSelf: 'center', marginBottom: 10 }}> Child Component </Text>
                    <View style={{ witdh: '100%', height: 50, flexDirection: 'row', justifyContent: 'space-around' }}>
                        <View style={{ width: '30%', backgroundColor: 'yellow', alignContent:'center', justifyContent:'center', alignItems:'center' }}>
                            <Text style={{ color: 'black', fontSize: 20 }}> Child </Text>
                        </View>
                        <View style={{ width: '30%', backgroundColor: 'yellow', alignContent:'center', justifyContent:'center', alignItems:'center' }}>
                            <Text style={{ color: 'black', fontSize: 20 }}> Child </Text>
                        </View>
                        <View style={{ width: '30%', backgroundColor: 'yellow', alignContent:'center', justifyContent:'center', alignItems:'center' }}>
                            <Text style={{ color: 'black', fontSize: 20 }}> Child </Text>
                        </View>
                        
                    </View>
                </View>


                <View style={{ width: '70%', height: 50, backgroundColor: 'magenta', marginBottom: 20, alignContent:'center', alignItems:'center', justifyContent:'center', alignSelf:'center'}}>
                    <Text style={{ color: 'white', fontSize: 20}}> child component</Text>
                </View>
                <View style={{ width: '70%', height: 50, backgroundColor: 'magenta', marginBottom: 20, alignContent:'center', alignItems:'center', justifyContent:'center', alignSelf:'center'}}>
                    <Text style={{ color: 'white', fontSize: 20}}> child component</Text>
                </View>





            </View>
        )
    }
}

export default Styling5
