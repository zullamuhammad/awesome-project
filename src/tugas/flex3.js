import React, {Component} from 'react'
import {Text, View, StyleSheet, TextInput, TouchableOpacity} from 'react-native'

class Flex3Exercise extends Component{
    render(){
        return(
            <View style={{flex: 1}}>
                <View style={{flex: 1, flexDirection: 'row'}}>
                    <View style={{flex: 1, backgroundColor: '#B82E2E'}}/>
                    <View style={{flex: 1, backgroundColor: '#23CB70'}}/>
                </View>

                <View style={{flex: 1, justifyContent: 'space-around', flexDirection: 'row'}}>
                    <View style={{width: '30%', backgroundColor: '#DC24E0'}}/>
                    <View style={{width: '30%', backgroundColor: '#DC24E0'}}/>
                </View>

                <View style={{flex: 1, flexDirection: 'row'}}>
                    <View style={{flex: 1, backgroundColor: '#2442E0'}}/>
                    <View style={{flex: 1, backgroundColor: '#32CB4A'}}/>
                    <View style={{flex: 1, flexDirection: 'column'}}>
                        <View style={{flex: 1, backgroundColor: '#DCE024'}}/>
                        <View style={{flex: 1, backgroundColor: '#E02424'}}/>
                    </View>
                </View>
            </View>
        )
    }
}


export default Flex3Exercise