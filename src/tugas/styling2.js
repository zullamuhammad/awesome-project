import React, { Component } from 'react'
import { Text, View } from 'react-native'

export class Styling2 extends Component {
    render() {
        return (
            <View style={{flex: 1, backgroundColor:'blue', padding:15}}>
                <View style={{width:'100%', alignItems: 'center', justifyContent:'center', marginBottom: 10}}>
                    <Text style={{color:'white', fontSize: 20}}> Parent Component </Text>
                </View>

                <View style={{width:'100%', height: 120, backgroundColor:'magenta', flexDirection:'row', alignSelf:'center', justifyContent:'space-between', padding: 10, marginBottom: 20}}>
                    <View style={{width: 100, height:100, backgroundColor:'yellow', borderRadius:50}}/>
                    <View style={{width:'60%', height :100, flexDirection:'column'}}>
                        <View style={{width: '100%', height: 30, backgroundColor:'green', marginBottom: 20}}/>
                        <View style={{width: '60%', height: 30, flexDirection:'row'}}>
                            <View style={{width: '60%', height: 30, backgroundColor:'red', marginRight: 10}}/>
                            <View style={{width: '60%', height: 30, backgroundColor:'yellow'}}/>
                        </View>
                    </View>
                </View>

                <View style={{width:'70%', height: 30, backgroundColor:'green', marginBottom: 20}}/>
                <View style={{width:'70%', height: 30, backgroundColor:'green', marginBottom: 20}}/>
                <View style={{width:'70%', height: 30, backgroundColor:'green', marginBottom: 20}}/>
                <View style={{width:'70%', height: 30, backgroundColor:'yellow', marginBottom: 20, alignSelf:'flex-end'}}/>
                <View style={{width:'70%', height: 30, backgroundColor:'yellow', marginBottom: 20, alignSelf:'flex-end'}}/>
                <View style={{width:'70%', height: 30, backgroundColor:'yellow', marginBottom: 20, alignSelf:'flex-end'}}/>
                
                
            </View>
        )
    }
}

export default Styling2
