import React, {Component} from 'react'
import {Text, View, StyleSheet, TextInput, TouchableOpacity} from 'react-native'

class Flex2Exercise extends Component{
    render(){
        return(
            <View style={{flex: 1}}>
                <View style={{flex: 1, flexDirection: 'row'}}>
                    <View style={{flex: 1, backgroundColor: '#B82E2E'}}/>
                    <View style={{flex: 1, backgroundColor: '#23CB70'}}/>
                </View>
                
                <View style={{flex: 1, flexDirection: 'row'}}>
                    <View style={{flex: 1, backgroundColor: '#32CBB9'}}/>
                    <View style={{flex: 1, backgroundColor: '#DC24E0'}}/>
                    <View style={{flex: 1, backgroundColor: '#32CBB9'}}/>

                </View>
            </View>
        )
    }
}


export default Flex2Exercise