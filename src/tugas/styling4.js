import React, { Component } from 'react'
import { Text, View } from 'react-native'

export class Styling4 extends Component {
    render() {
        return (
            <View style={{flex: 1, backgroundColor:'blue', padding: 20}}>
                <View style={{flex: .05, justifyContent: 'center', alignItems: 'center', marginBottom: 10}}>
                    <Text style={{color:'white', fontSize: 17 }}> Parent Component </Text>
                </View>
                
                <View style={{width: '70%', height: 120, backgroundColor:'yellow', alignItems:'center', justifyContent:'center', alignSelf:'center', marginBottom: 20}}/>
                <View style={{width:'100%', height: 30, backgroundColor:'green', alignItems:'center', justifyContent:'center', marginBottom: 20}}/>
                <View style={{width:'100%', height: 30, backgroundColor:'green', alignItems:'center', justifyContent:'center', marginBottom: 20}}/>
                <View style={{width: '40%', height: 30, backgroundColor:'red', alignSelf:'flex-end'}}/>
            </View>
        )
    }
}

export default Styling4
