import React, { Component } from 'react'
import { Text, View } from 'react-native'

export class Styling1 extends Component {
  render() {
    return (
      <View style={{flex: 1, backgroundColor:'blue', padding: 15}}>
        <View style={{flex: .05, justifyContent: 'center', alignItems: 'center', marginBottom: 20}}>
          <Text style={{color:'white', fontSize: 17 }}> Parent Component </Text>
        </View>
        
        <View style={{width:'100%', height: 30, flexDirection:'row'}}>
          <View style={{width:'40%', backgroundColor:'magenta'}}/>
          <View style={{width:'40%', backgroundColor:'green'}}/>
        </View>

        <View style={{width:'100%', height: 30, flexDirection:'row', marginVertical: 20, justifyContent:'space-between'}}>
          <View style={{width:'40%', backgroundColor:'magenta'}}/>
          <View style={{width:'40%', backgroundColor:'green'}}/>
        </View>

        <View style={{witdh:'100%', height: 200, backgroundColor:'magenta', padding: 10}}>
          <View style={{width:'100%', height: 30, justifyContent:'space-between', flexDirection:'row', marginVertical:10}}>
            <View style={{width:'40%', backgroundColor: 'green'}}/>
            <View style={{width:'40%', backgroundColor: 'green'}}/>
          </View>

          <View style={{width:'100%', height: 30, flexDirection:'row', marginVertical: 20}}>
            <View style={{width:'40%', backgroundColor: 'green'}}/>
            <View style={{width:'40%', backgroundColor: 'yellow'}}/>
          </View>

          <View style={{width:'100%', height: 30, flexDirection:'row', justifyContent:'center'}}>
            <View style={{width:'20%', backgroundColor:'yellow'}}/>
            <View style={{width:'20%', backgroundColor:'green'}}/>
            <View style={{width:'20%', backgroundColor:'red'}}/>
          </View>
        </View>
      
      </View>
    )
  }
}

export default Styling1
